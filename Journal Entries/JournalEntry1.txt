October 14th, 2016:
By Michael Davies.

Course Expectations:

I expected Javascript to be sunshine and rainbows in the browsers but it more like a large pile of syntax errors one after another. That being said, I thrive on the challege 
to tackle the problems in the most effectice manner. I have surpassed all of my expectations coming into the course as I have learnt more than I ever thought I would have been
able to accomplish in this short of time. 


How you feel about the learning pace:

The pace is fast at times and I am just able to complete the exercises just in time. Sometimes if I come across an error, I fall behind and rely on other students code to get
caught up. Unfortunately regardless of doing all the assigned readings and W3 exercises for each class, I still feel slightly ill-prepared for the lectures and I am not sure
whether it is just a lack of being able to conceptualize Javascript as a whole or just simply needing more practice. I am dedicating the entire weekend to practicing and 
trying to get up to speed so-to-say to try and wrap my mind around everything we have covered this week. 


Any questions you want to ask:

Would code academy tutorials help me expand my knowledge on JS? If so, are they any exercises in particular you would suggest doing for extra practice?

