function getRandomGamePiece(){

    var gamePieces = ["Rock", "Paper", "Scissors", "Dynamite"];
    var choice = prompt("rock, paper, scissors, or dynamite? [All lowercase please]");
    var piece;    
    var randomIndex1 = (Math.ceil(Math.random() * gamePieces.length)-1);
        randomPiece = gamePieces[randomIndex1];

        switch(randomIndex1){
            case 0:  
                piece = "Rock";
                break;
            case 1:  
                piece = "Paper";
                break;
            case 2:  
                piece = "Scissors";
                break;
            case 3:
                piece = "Dynamite";
                break;
        }

    $('#aiChoice').text("The A.I drew a " + piece + ".");
    whoWins();

function whoWins(){
        if(choice=="rock"  && piece=="Paper"){
            $('#results').text("You lose. Please try again");
        }
        if(choice=="rock"  && piece=="Dynamite"){
            $('#results').text("You lose. Please try again");
        }
        if(choice=="rock"  && piece=="Scissors"){
            $('#results').text("You win! Woohoo!");
        }
        if(choice=="paper" && piece=="Dynamite"){
            $('#results').text("You lose. Please try again");
        }
        if(choice=="paper" && piece=="Scissors"){
            $('#results').text("You lose. Please try again");
        }
        if(choice=="scissors" && piece=="Dynamite"){
            $('#results').text("You win! Woohoo!");
        }
        if(choice=="scissors" && piece=="Scissors"){
            $('#results').text("Tie. Please try again.");
        }
        if(choice=="dynamite" && piece=="Dynamite"){
            $('#results').text("Tie. Please try again.");
        }
        if(choice=="rock" && piece=="Rock"){
            $('#results').text("Tie. Please try again.");
        }
        if(choice=="paper" && piece=="Paper"){
            $('#results').text("Tie. Please try again.");
        }
    }
}