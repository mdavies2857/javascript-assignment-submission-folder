// myScript.js file - Oct.14th, 2016 || Michael Davies (000751183)
           
// Function that has an onload event on the body of the html document
function loadProvinces(){

    // Variables
    var subjectArray = ["-Select-","Alberta", "British Columbia", "Ontario", "Manitoba", "Saskatchewan", "Quebec", "New Brunswick", "Nova Scotia", "Prince Edward Island", "Newfoundland and Labrador"];
    var optionOutput="";

    // Loop to create select box and the options within
    for(subIndex=0;subIndex<subjectArray.length;subIndex++){
        optionOutput+="<option name='subject" + subIndex + "' value='" + subjectArray[subIndex] + "'>" + subjectArray[subIndex] + "</option>";
    }
    document.getElementById("sBox").innerHTML=optionOutput;
}

// Validation Function for select box and the two text boxes
function validateForm(){
    if(document.forms[0].elements[0].selectedIndex==0){
        alert("Please select a province.");
        document.forms[0].elements[0].style.backgroundColor='#b6be40';                    
        document.forms[0].elements[0].focus();
        return false; 
    
    }else if(document.forms[0].elements[1].value==""){
        alert("Please enter your name.");
        document.forms[0].elements[1].style.backgroundColor='#b6be40';
        document.forms[0].elements[1].focus();
        return false; 

    }else if(document.forms[0].elements[2].value==""){
        alert("Please enter your e-mail.");
        document.forms[0].elements[2].focus();
        document.forms[0].elements[2].style.backgroundColor='#b6be40';
        return false; 

    }
    document.forms[0].submit();  
}
